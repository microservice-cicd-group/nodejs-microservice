import * as amqp from 'amqplib';

async function connectMessage() {
    try {
        const connection = await amqp.connect('amqp://guest:guest@localhost');
        const channel = await connection.createChannel();
        console.log('connected to message broker !');
        return channel
    } catch (error) {
        console.log(error)
    }
}

connectMessage();