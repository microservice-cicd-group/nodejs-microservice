// Producer
import * as amqp from 'amqplib';

async function producerTicketCreated(params: any) {
    const connection = await amqp.connect('amqp://localhost');
    const channel = await connection.createChannel();

    const queueName = 'ticket:created';
    await channel.assertQueue(queueName);

    const event = {
        id: params.id,
        title: params.title,
        price: params.price,
        userId: params.userId,
        version: params.version
    };
    channel.sendToQueue(queueName, Buffer.from(JSON.stringify(event)));

    console.log('Event sent:', event);
    await channel.close();
    await connection.close();
}

export default producerTicketCreated