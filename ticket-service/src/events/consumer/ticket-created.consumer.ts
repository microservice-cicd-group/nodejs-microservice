// Consumer
import * as amqp from 'amqplib';

async function consumerTicketCreated() {
    const connection = await amqp.connect('amqp://localhost');
    const channel = await connection.createChannel();

    const queueName = 'ticket:created';
    await channel.assertQueue(queueName);

    channel.consume(queueName, (message: any) => {
        const event = JSON.parse(message.content.toString());
        console.log('Received event:', event);
        // Process the event (e.g., send email, update database)
    }, { noAck: true });
}

export default consumerTicketCreated