import mongoose from 'mongoose';
import * as amqp from 'amqplib';
import { app } from './app';
import { URL_DB } from './config/db.config';


const start = async () => {
    // if (!process.env.JWT_KEY) {
    //     throw new Error('JWT_KEY must be defined');
    // }
    // if (!process.env.MONGO_URI) {
    //     throw new Error('MONGO_URI must be defined');
    // }
    // if (!process.env.NATS_CLIENT_ID) {
    //     throw new Error('NATS_CLIENT_ID must be defined');
    // }
    // if (!process.env.NATS_URL) {
    //     throw new Error('NATS_URL must be defined');
    // }
    // if (!process.env.NATS_CLUSTER_ID) {
    //     throw new Error('NATS_CLUSTER_ID must be defined');
    // }

    try {
        // Connect RabbitMQ
        const connection = await amqp.connect('amqp://guest:guest@localhost');
        await connection.createChannel();
        console.log('connected to message broker !');
        // Connect DB 
        await mongoose.connect(URL_DB);
        console.log('Connected to MongoDb');
    } catch (error) {
        console.log(error)
    }

    app.listen(3002, () => {
        console.log('Listening on port 3002!!!!!!!!');
    });
};

start();
