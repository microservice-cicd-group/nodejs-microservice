import { Request, Response, NextFunction } from 'express';
import * as jwt_decode from 'jwt-decode';

interface UserPayload {
  id: string;
  email: string;
}

declare global {
  namespace Express {
    interface Request {
      currentUser?: UserPayload;
    }
  }
}

export const currentUser = (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const HEADERS = 'authorization';
  try {
    if (!!req.headers[HEADERS]) {
      const bearerHeader = req.headers[HEADERS].toString();
      const bearer = bearerHeader.split(' ');
      const bearerToken = bearer[1];

      const result = (
        jwt_decode.jwtDecode(bearerToken)
      ) as UserPayload;
      req.currentUser = result
    }
  } catch (error) {
    throw error
  }

  next();
};
