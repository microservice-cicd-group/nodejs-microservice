// Consumer
import * as amqp from 'amqplib';

async function consumeEvent() {
    const connection = await amqp.connect('amqp://localhost');
    const channel = await connection.createChannel();

    const queueName = 'my_events';
    await channel.assertQueue(queueName);

    channel.consume(queueName, (message) => {
        const event = JSON.parse(message.content.toString());
        console.log('Received event:', event);
        // Process the event (e.g., send email, update database)
    }, { noAck: true });
}



consumeEvent();