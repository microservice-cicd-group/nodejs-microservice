// Producer
import * as amqp from 'amqplib';

async function produceEvent() {
    const connection = await amqp.connect('amqp://localhost');
    const channel = await connection.createChannel();

    const queueName = 'my_events';
    await channel.assertQueue(queueName);

    const event = { type: 'order_placed', data: { orderId: 123 } };
    channel.sendToQueue(queueName, Buffer.from(JSON.stringify(event)));

    console.log('Event sent:', event);
    await channel.close();
    await connection.close();
}

produceEvent();

