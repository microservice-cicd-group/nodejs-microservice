import express from 'express';
import bodyParser from "body-parser";
import cors from "cors";
import mongoose from 'mongoose';
import { URL_DB } from '../src/config/db.config'
import router from './router/user.router'
import cookieSession from 'cookie-session';

const app = express()
const port = 3001

app.get('/', (req: any, res: any) => {
    res.send('App is running')
})
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(router)
app.use(
    cookieSession({
        signed: false,
        secure: false,
    })
);

app.set('trust proxy', true);

try {
    mongoose.connect(URL_DB);
    console.log('Connected to MongoDb');
} catch (err) {
    console.error(err);
}

app.listen(port, () => {
    console.log(`Example app listening on port ${port}`)
})
app.use(cors())

export { app }