import express, { Request, Response } from 'express';
import { body } from "express-validator";
import { UserService } from '../service/user.service';
import { validateRequest } from '../middleware/validate-request';
import { currentUser } from '../middleware/current-user';
import * as jwt_decode from 'jwt-decode';



const router = express.Router();
const user = new UserService();

router.post(
    '/api/users/signup',
    [
        body('email')
            .isEmail()
            .withMessage('Email must be valid'),
        body('password')
            .trim()
            .isLength({ min: 4, max: 20 })
            .withMessage('Password must be between 4 and 20 characters')
    ],
    // validateRequest,
    async (req: Request, res: Response) => {
        const data = await user.signup(req.body);
        return res.status(201).send(data);
    }
);


router.post(
    '/api/users/login',
    [
        body('email')
            .isEmail()
            .withMessage('Email must be valid'),
        body('password')
            .trim()
            .isLength({ min: 4, max: 20 })
            .withMessage('Password must be between 4 and 20 characters')
    ],
    // validateRequest,
    async (req: Request, res: Response) => {
        const data: any = await user.login(req.body);
        return res.status(201).send(data);
    }
);


router.get('/api/users/currentuser', currentUser, (req, res) => {
    const HEADERS = 'authorization';
    try {
        if (!!req.headers[HEADERS]) {
            const bearerHeader = req.headers[HEADERS].toString();
            const bearer = bearerHeader.split(' ');
            const bearerToken = bearer[1];

            const result = jwt_decode.jwtDecode(bearerToken);
            res.send({ result });
        }
    } catch (error) {
        throw error
    }
});


export default router;