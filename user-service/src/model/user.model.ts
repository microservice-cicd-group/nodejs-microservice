import mongoose from 'mongoose'
import UserSecurity from '../security/user.security';

interface UserAttrs {
    username: string;
    password: string;
}

// An interface that describes the properties
// that a User Model has
interface UserModel extends mongoose.Model<UserDoc> {
    build(attrs: UserAttrs): UserDoc;
}

// An interface that describes the properties
// that a User Document has
interface UserDoc extends mongoose.Document {
    username: string;
    password: string;
}

const userSchema = new mongoose.Schema({
    username: String,
    fulname: String,
    email: String,
    phoneNumber: String,
    address: String,
    password: String,
})


userSchema.statics.build = (attrs: UserAttrs) => {
    return new User(attrs);
};

const User = mongoose.model<UserDoc, UserModel>('User', userSchema);

export { User };