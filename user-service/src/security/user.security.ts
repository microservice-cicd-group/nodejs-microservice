import CryptoJS from 'crypto-js';
import PASS_SECRET from '../config/auth.config';

const pass_secret: any = new PASS_SECRET();
class UserSecurity {
    public encrypt(password: string): string {
        return CryptoJS.AES.encrypt(password, "abcd").toString()
    }

    public decrypt(password: string): string {
        const decrypt = CryptoJS.AES.decrypt(password, "abcd").toString(CryptoJS.enc.Utf8)
        return decrypt
    }

    public comparePassword(password: string, decryptedPassword: string): boolean {
        return password === this.decrypt(decryptedPassword)
    }
}

export default UserSecurity
