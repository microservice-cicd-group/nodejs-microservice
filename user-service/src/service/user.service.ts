import { User } from "../model/user.model";
import jwt from 'jsonwebtoken';
import UserSecurity from "../security/user.security";
import JWT_KEY from '../config/auth.config'


const userSecurity = new UserSecurity();
const jwtKey = new JWT_KEY();
export class UserService {
    constructor() {

    }

    async signup(params: any) {
        const { username, password } = params;

        const existingUser = await User.findOne({ username });

        if (existingUser) {
            throw 'Username in use';
        }

        const user = User.build({ username, password });
        user.password = await userSecurity.encrypt(password);
        await user.save();

        return user
    }

    async login(params: any) {
        try {
            const user = User.findOne({
                username: params.username,
            });
            const foundUser = await user.exec();
            if (!foundUser) {
                throw "Error"
            } else {
                const checkPass = userSecurity.comparePassword(params.password, foundUser.password);
                if (checkPass === false) {
                    return "Invalid Password"
                } else {
                    const token = jwt.sign(
                        {
                            id: foundUser.id,
                            username: foundUser.username
                        },
                        jwtKey.JWT_KEY!
                    );
                    return { data: foundUser, access_token: token }
                }
            }
        } catch (error) {
            console.log(error)
        }
    }
}