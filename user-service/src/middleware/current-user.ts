import { Request, Response, NextFunction } from 'express';
import jwt from 'jsonwebtoken';

import JWT_KEY from '../config/auth.config'

interface UserPayload {
    id: string;
    email: string;
}

declare global {
    namespace Express {
        interface Request {
            currentUser?: UserPayload;
        }
    }
}

export const currentUser = (
    req: Request,
    res: Response,
    next: NextFunction
) => {
    const jwtKey = new JWT_KEY();

    if (!req.session?.access_token) {
        return next();
    }

    try {
        const payload = jwt.verify(
            req.session?.access_token,
            jwtKey.JWT_KEY!
        ) as UserPayload;
        req.currentUser = payload;
    } catch (err) { }

    next();
};
